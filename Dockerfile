FROM quay.io/centos7/redis-6-centos7:latest
USER root

RUN yum install epel-release -y && yum install python3 python3-redis -y && yum clean all

RUN sed -i 's/dir \/var\/lib\/redis\/data/dir \/tmp/' /etc/redis.conf

# Set the working directory in the container
WORKDIR /projects
RUN chgrp -R 0 /projects && \
    chmod -R g=u /projects

# Copy the dependencies file to the working directory
COPY requirements.txt .

# Install any dependencies
# RUN python3 -m pip install -r requirements.txt

# Copy the content of the local src directory to the working directory
COPY . .

# Specify the command to run on container start
CMD [ "python3", "./app.py" ]