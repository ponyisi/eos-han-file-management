import redis
import subprocess
import glob

rsrv = subprocess.Popen(['redis-server', '/etc/redis.conf'])

while True:
    l = glob.glob('/eos/atlas/atlascerngroupdisk/data-dqm/han_results/tier0/collisions/*/*/run_*')
    r = redis.Redis()
    r.sadd('han:tier0', *l)
    import time
    time.sleep(60)